import React, { Component } from 'react';
import { Image, Platform, StatusBar, MapView, TouchableOpacity } from 'react-native';
import { Container, Header, Content, Text, Item, Input, Button, Icon, View, Toast, Row, Grid, Col } from 'native-base';
import { Field, reduxForm } from 'redux-form';
import styles from './styles';

const commonColor = require('../../theme/variables/commonColor');
const backgroundImage = require('../../../assets/glow2.png');
const logo = require('../../../assets/logo.png');

class EwsBase extends Component {
	watchID = null;

	constructor(props) {
		super();
		this.state = {
			latitude: null,
			longitude: null,
			error: null,
		};
	}

	componentDidMount() {
		this.watchId = navigator.geolocation.watchPosition(
			(position) => {
			  this.setState({
				latitude: position.coords.latitude,
				longitude: position.coords.longitude,
				error: null,
			  });
			},
			(error) => this.setState({ error: error.message }),
			{ enableHighAccuracy: true, timeout: 20000, maximumAge: 1000, distanceFilter: 10 },
		  );
	}

	componentWillUnmount() {
		navigator.geolocation.clearWatch(this.watchId);
	}

	render() {
		return (
			<Container>
				<Image source={backgroundImage} style={styles.containerImage}>
					<Image source={logo} style={styles.logoShadowImage}>
						<Content>
							<Grid>
								<View style={styles.bgView}>
									<Row size={1}>
										<Icon name="md-locate" />
									 

									{this.state.latitude? 	<Text>  {this.state.latitude.toFixed(5)}</Text> : null }
									{this.state.longitude? 	<Text>  {this.state.longitude.toFixed(5)}</Text> : null}
										 
									</Row>

									<Row style={{ height: 19 }} />
									<Row size={2}>
										<Col style={{ margin: 7 }}>
											<Icon name="md-archive" />
											<Text>Reports</Text>
										</Col>

										<Col style={{ margin: 7 }}>
											<Icon name="md-create" />
											<Text>Submit Report</Text>
										</Col>
										<Col style={{ margin: 7 }}>
											<Icon name="md-home" />
											<Text>Login</Text>
										</Col>
									</Row>

									<Row style={{ height: 35 }} />
									<Row size={2}>
										<Col style={{ margin: 7 }}>
											<Icon name="md-information" />
											<Text>About</Text>
										</Col>
										<Col style={{ margin: 7 }}>
											<Icon name="md-add" />
											<Text>Signup</Text>
										</Col>
										<Col style={{ margin: 7 }}>
											<Icon name="md-mail" />
											<Text>Contact Us</Text>
										</Col>
									</Row>
								</View>
							</Grid>
						</Content>
					</Image>
				</Image>
			</Container>
		);
	}
}

export default EwsBase;
